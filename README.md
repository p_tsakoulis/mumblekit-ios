# mumblekit-ios

[![CI Status](https://img.shields.io/travis/pantelis/mumblekit-ios.svg?style=flat)](https://travis-ci.org/pantelis/mumblekit-ios)
[![Version](https://img.shields.io/cocoapods/v/mumblekit-ios.svg?style=flat)](https://cocoapods.org/pods/mumblekit-ios)
[![License](https://img.shields.io/cocoapods/l/mumblekit-ios.svg?style=flat)](https://cocoapods.org/pods/mumblekit-ios)
[![Platform](https://img.shields.io/cocoapods/p/mumblekit-ios.svg?style=flat)](https://cocoapods.org/pods/mumblekit-ios)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

mumblekit-ios is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'mumblekit-ios'
```

## Author

pantelis, p.tsakoulis@dga.gr

## License

mumblekit-ios is available under the MIT license. See the LICENSE file for more info.
