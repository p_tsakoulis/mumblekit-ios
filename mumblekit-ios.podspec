#
# Be sure to run `pod lib lint mumblekit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'mumblekit-ios'
  s.version          = '0.1.3'
  s.summary          = 'MumbleKit ios.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Mainly created to use with flutter
                       DESC

  s.homepage         = 'https://bitbucket.org/p_tsakoulis/mumblekit-ios'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'pantelis tsak' => 'p.tsakoulis@dga.gr' }
  s.source           = { :git => 'https://p_tsakoulis@bitbucket.org/p_tsakoulis/mumblekit-ios.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'
  
  # s.resource_bundles = {
  #   'mumblekit' => ['mumblekit/Assets/*.png']
  # }

  s.vendored_libraries = 'mumblekit-ios/libs/libMumbleKit.a'
  s.public_header_files = 'mumblekit-ios/Headers/Public/*.h'
  s.source_files = ['mumblekit-ios/Headers/**/*.h', 'mumblekit-ios/Classes/**/*']
  
  s.frameworks = 'AudioToolbox', 'CFNetwork', 'Security', 'UIKit'
  s.library = 'c++'
  s.swift_version = '5.0'
  
  s.pod_target_xcconfig = {
    'OTHER_LDFLAGS' => '-ObjC'
  }
  
#  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
#  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end
